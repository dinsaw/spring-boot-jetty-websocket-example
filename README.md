# Spring-Boot-Jetty-WebSocket-Example

This is basic example of how to configure Jetty WebSocket with Spring Boot. 
For more information read my blog post - [Jetty WebSocket with Spring Boot](http://www.dineshsawant.com/jetty-websocket-with-spring-boot/)
